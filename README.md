# Health & Fitness Tracker App

**Hi Ivan, thanks for having a look over this repo!**

  This is an app I created while I was studying for my App Builder exam.  If you would like to run it in a scratch org, here are a couple of extra things to do...

After you clone, create a scratch org, and push the metadata you can run this script:
 

    sh ./scripts/sh/finaliseDeployment.sh *yourScratchOrgAlias*

**or** just run these three commands:

    sfdx force:apex:execute -f ./scripts/apex/assignPermissionSet.apex
    sfdx force:apex:execute -f ./scripts/apex/setupNewGoal.apex
    sfdx force:data:tree:import -f data-import/Food_Item__cs.json -u *yourScratchOrgAlias*


## Explore
Navigate to Health & Fitness Tracker App.  Here are the tab explanations:

**Home** was designed to house the reports and dashboards I would later create once I had some data entered into the app.  

**Gym Session** is a flow, it creates individual records for each type of exercise completed for `today()`. The check boxes are just a visual representation of what exercises has been completed so far.  The picklist below defaults to the next exercise in the list, but you can skip ahead (if someone is on you machine) and it will always default to the next uncompleted exercise on the list next time it is ran.

If you complete the same exercise twice, (regardless of if is the same day, or another day) it will show you the previous values to compete against.

**Weight Tracker** lets you imput todays figures, it is stored on the `Training Day` object under the record created for `today()`.

**Training Goal** lets you set up a goal.  There can only be one active goal (a Flow insures this), and once you input the required data, a trigger will create a certain amount of training days - days required to achieve this goal. This is calculated on some interesting formula fields within the `Training Goal` object.  All the maths in the app was taken from a diet and exercise book I read.

**Training Days** lets you see the days automatically created by the trigger.  The list view (same as all these objects) lets you toggle between days for current goal, or today. You can create detail cardio records for each day, or detail gym records if you don't want to use the Weight Tracker Flow Tab.

**Food Diary Entries** lets you create a meal plan that must be linked to a `Training Goal`, but can also be linked to a `Training Day`. **Note:** you must have `Food Items` set up, however some have been provided.

**Food Items** contain a list of foods.  The multiplier means you can add an item using the standard caloric details for 100g and multiply it by what ever you need.  Example: 40g of an item is multiplied by 0.4 and a 250g item is multiplied by 2.5.

**Weight Training Sessions & Cardio Sessions** are just a list of completed sessions that are linked to a `Training Day`.

**Schema Builder:**


![Objects Layout](./resources/layout.jpg)