### Finalise Setup ###

# Give access to the app to the Administrator
sfdx force:apex:execute -f ./scripts/apex/assignPermissionSet.apex

# Creates a demo training goal to demonstrate Master/Detail relationship between Goal and Days objects. 
sfdx force:apex:execute -f ./scripts/apex/setupNewGoal.apex

# Add some basic data to Food Items.
sfdx force:data:tree:import -f data-import/Food_Item__cs.json -u $1