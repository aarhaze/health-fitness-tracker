trigger UpdateEndDate on Training_Goal__c (before insert) {

    for (Training_Goal__c tg : Trigger.new) {
        
        Decimal daysToCompletion = tg.Deficit_Duration__c * 7;
        tg.End_Date__c = tg.Start_Date__c.addDays(Integer.valueOf(daysToCompletion));
    }
}