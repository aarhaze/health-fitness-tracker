trigger CreateTrainingDays on Training_Goal__c (after insert) {

    List<Training_Day__c> tdList = new List<Training_Day__c>();

    for (Training_Goal__c tg : Trigger.new) {

        Integer recsToCreate = tg.Start_Date__c.daysBetween(tg.End_Date__c);

        for (Integer i = 0; i < recsToCreate; i++) {
            Training_Day__c td = new Training_Day__c(
                Training_Goal__c = tg.Id,
                Date__c = tg.Start_Date__c.addDays(i)
            );
            tdList.add(td);
        }
    }

    if (!tdList.isEmpty()) {
        insert tdList;
    }
}