@IsTest
public with sharing class UpdateEndDateTrigger_Test {

    @IsTest static void setEndDate(){
        Date startDate = Date.newInstance(2021, 10, 01);
        Date endDate   = Date.newInstance(2021, 10, 22);

        Training_Goal__c tg = new Training_Goal__c( 
            Name = 'Test',
            Start_Date__c = startDate,
            Starting_Weight__c = 100,
            Starting_Body_Fat__c = 12
        );
        insert tg;

        Training_Goal__c updatedTg = [SELECT Id, 
                                             Start_Date__c,   
                                             End_Date__c 
                                        FROM Training_Goal__c
                                       WHERE Id = :tg.Id];                                          

        System.assertEquals(endDate, updatedTg.End_Date__c);     
    }
}